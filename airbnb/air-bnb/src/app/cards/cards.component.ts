import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
//import { Description } from '@material-ui/icons';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  
  @Input () src : any;
  @Input () title :any;
  @Input () description :any;
  @Input () price :any;

  constructor( ) { }

  ngOnInit(): void {
  }

}
