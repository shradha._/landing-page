import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {


  public form: FormGroup;
  public submitted = false;
 
  date = new Date(2020, 1, 1);

  minDate = new Date(2000, 0, 1);
  maxDate = new Date(2020, 11, 1);
  constructor(
    fb: FormBuilder,
  ) { 
    this.form = fb.group({
      insurance_expiry_date: ['', Validators.compose([Validators.required])],
     
    });
 
  }

  onSubmit(form) {
    const body = {
      insurance_expiry: form.insurance_expiry_date,
    }
  }

  ngOnInit(): void {
  }

}
