
import { NgModule } from '@angular/core';
//import {     MatDateRangePicker } from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule} from '@angular/material/input';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ImportContacts } from '@material-ui/icons';

@NgModule({
  imports: [
    MatDatepickerModule,
    MatFormFieldModule,
    //MatDateRangePicker,
    MatInputModule,
    BrowserAnimationsModule,
    
  ],
  exports: [
    MatDatepickerModule,
    MatFormFieldModule,
    //MatDateRangePicker,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [ MatDatepickerModule ],
})

export class MaterialModule {}