import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule} from '@angular/material/icon';
import { BannerComponent } from './banner/banner.component'
import { MaterialModule} from './material';
import { MatButtonModule } from '@angular/material/button';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CardsComponent } from './cards/cards.component';
import { FooterComponent } from './footer/footer.component';
//import { CompilerConfig } from '@angular/compiler';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    BannerComponent,
    SearchComponent,
    CardsComponent,
    FooterComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    //MaterialModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    //MatDateRangePicker,
    MatInputModule,
  
  ],
  exports:[//MatIconModule,
            MatButtonModule,
  ],
  providers: [ MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
