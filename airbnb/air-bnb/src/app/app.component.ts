import { Component, NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { HomeComponent } from "./home/home.component";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// @NgModule({
//   imports:[MatIconModule],
//   exports:[MatIconModule]
//})
export class AppComponent {
  //title = 'air-bnb';
}
